![Hirslanden Logo](http://www.hirslanden.ch/content/dam/global/Allgemeine%20Seite/Files/mailfooter/hir_logo_claim_de.png)

# Dokumentation Yien Tut

## GitHub

**Command line instructions**
```
Git global setup
git config --global user.name "Näne Yien Tut"
git config --global user.email "naene.yientut@outlook.com"
```
**Create a new repository**
```
git clone git@gitlab.com:naeneyt/BIST18-M300.git
cd BIST18-M300
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```
**Existing folder**
```
cd existing_folder
git init
git remote add origin git@gitlab.com:naeneyt/BIST18-M300.git
git add .
git commit -m "Initial commit"
git push -u origin master
```
**Existing Git repository**
```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:naeneyt/BIST18-M300.git
git push -u origin --all
git push -u origin --tags
```

## Vagrant

**1. To rebuild this VM create an empty directory somewhere and cd into it.**

**Then execute the following commands:**

```
vagrant init ubuntu/trusty64 <- Box Name Goes here !!
```

**2. Open your Vagrant File and paste the following Code into it**

```
Vagrant.configure("2") do |config|
  config.vm.define "APPSRV001" do |subconfig|
    subconfig.vm.box = "ubuntu/trusty64"
  end

config.vm.hostname = "APPSRV001"

####FOLDER SETTINGS###
config.vm.synced_folder "../data/html", "/home/vagrant/html"

####PROVISION SETTINGS###
$script = <<-SCRIPT
mkdir ~/hmtl
apt-get install -y apache2
cp -f /home/vagrant/html/index.html /var/www/html
SCRIPT
config.vm.provision "shell", inline: $script

#####BOX SETTINGS###
config.vm.box = "ubuntu/trusty64"

####PROVIDER SETTINGS###
config.vm.provider "virtualbox" do |vb|
end

#####NETWORK SETTINGS###
config.vm.network "private_network", ip: "192.168.0.10"
end
```

**3. Create your index.html File. Create a new folder structure in Your Directory like so YOURDIRECTORY\data\html and and paste it in.**


**4. Start your VM with the following Command**

```
vagrant up
```

**Congratulations you have successfully rebuilt my webserver!**

## A couple of tipps to use control your VM

**How to suspend/resume your VM**

```
vagrant suspend <- Suspends your VM
vagrant resume <- Resumes your VM
```

**Setting Hostname for VM (Vagrantfile)**

```
config.vm.hostname = "SERVERNAME"
```

**Shell script to be executed (Vagrantfile)**

```
$script = <<-SCRIPT
<<YOUR COMMANDS GO HERE>>
SCRIPT
config.vm.provision "shell", inline: $script
```

**Configure additional Private Network Interface (Vagrantfile)**

```
config.vm.network "private_network", ip: "192.168.0.10"
```

**Working with Synced directories between host and guest**

```
config.vm.synced_folder "path/on/host", "path/on/guest"
```

**Created by Näne Yien Tut**